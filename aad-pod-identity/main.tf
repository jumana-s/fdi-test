locals {
  mc_resource_group = "MC_${var.cluster_resource_group_name}_${var.cluster_name}_${var.cluster_location}"
}

data "azurerm_kubernetes_cluster" "cluster" {
  name                = var.cluster_name
  resource_group_name = var.cluster_resource_group_name
}

data "azurerm_resource_group" "mc_rg" {
  name = local.mc_resource_group
}

resource "azurerm_role_assignment" "aad_pod_identity_velero_operator" {
  scope                = data.azurerm_resource_group.mc_rg.id
  role_definition_name = "Managed Identity Operator"
  principal_id         = data.azurerm_kubernetes_cluster.cluster.kubelet_identity[0].object_id // var.kubernetes_identity_object_id
}

resource "azurerm_role_assignment" "vmc" {
  scope                = data.azurerm_resource_group.mc_rg.id
  role_definition_name = "Virtual Machine Contributor"
  principal_id         = data.azurerm_kubernetes_cluster.cluster.kubelet_identity[0].object_id
}

resource "azurerm_user_assigned_identity" "mi" {
  resource_group_name = local.mc_resource_group
  location            = var.cluster_location

  name = "demo"
}

resource "helm_release" "identity" {
  name    = "aad-pod-identity"
  chart   = "https://github.com/Azure/aad-pod-identity/raw/master/charts/aad-pod-identity-4.1.6.tgz"
  version = "4.1.6"
  wait    = false
  values = [
    <<EOF
azureIdentities:
- name: "${azurerm_user_assigned_identity.mi.name}"
  type: 0
  resourceID: "${azurerm_user_assigned_identity.mi.id}"
  clientID: "${azurerm_user_assigned_identity.mi.client_id}"
  binding:
    name: "${azurerm_user_assigned_identity.mi.name}-binding"
    selector: "${azurerm_user_assigned_identity.mi.name}"
EOF
  ]
}


# Role assignment should be done by FDI

# ADDING ROLE ASSIGNMENT
# Click add role assignment,
# use role Storage Account Key Operator Service Role https://docs.microsoft.com/en-us/azure/role-based-access-control/built-in-roles#storage-account-key-operator-service-role
# in members assign access to managed identity then select correct member
# CREATE!
