package httpapi.authz

import input

default allow = false

rl_permissions := {
	"kubeflow_reader": [
		{"action": "s3:ListBucket"},
		{"action": "s3:GetObject"},
		{"action": "s3:ListAllMyBuckets"},
	],
	"kubeflow_writer": [
		{"action": "s3:DeleteObject"},
		{"action": "s3:GetObject"},
		{"action": "s3:GetBucketObjectLockConfiguration"},
	],
}

allow {
	input.account == data[input.bucket].readers[_]
	permissions := rl_permissions.kubeflow_reader
	p := permissions[_]
	p == {"action": input.action}
}

allow {
	input.account == data[input.bucket].writers[_]
	permissions := rl_permissions.kubeflow_writer
	p := permissions[_]
	p == {"action": input.action}
}
